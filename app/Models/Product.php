<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $guarded = [];

    public $timestamps = true;


    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function ProductImage()
    {
        return $this->hasMany(ProductImage::class, 'product_id');
    }

    public function ProductOption()
    {
        return $this->hasMany(ProductOption::class, 'product_id');
    }


    /**
     * @param $user_id
     * @param $product_id
     * @return bool
     */
    public static function isFav($user_id, $product_id)
    {
        $is_fav = Fav::where('user_id', $user_id)->where('product_id', $product_id)->first();
        if ($is_fav)
            return true;
        else
            return false;
    }
}
