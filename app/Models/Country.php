<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    protected $guarded = [];

    public $timestamps = true;
    public function cities()
    {
        return $this->hasMany(City::class, 'country_id');
    }
}
