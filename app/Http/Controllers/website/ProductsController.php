<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\City;
use App\Models\Country;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductOption;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;

class ProductsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchFilter(Request $request)
    {


        $products = Product::whereHas('user');

        if ($request->city_id) {
            $products = $products->where('city_id', $request->city_id);
        }
        if ($request->category_id) {
            $ids = [];
            $category = Category::find($request->category_id);
            $children = Category::where('parent_id', $category->id)->get();
            foreach ($children as $child) {
                $temp = Category::where('parent_id', $child->id)->pluck('id')->toArray();
                array_push($ids,$temp);
            }

            $products = $products->whereIn('category_id', $ids);
        }
        if ($request->subcategory_id) {
            $ids = Category::where('parent_id', $request->subcategory_id)->pluck('id')->toArray();
            $products = $products->whereIn('category_id', $ids);
        }

        if ($request->type) {
            $products = $products->where('category_id', $request->type);
        }
        if ($request->option_values) {
            foreach ($request->option_values as $key => $value)
            {
                if ($value!='')
                {
                    $option_id = $request->option_ids[$key];
                    $products = $products->whereHas('ProductOption', function ($q) use ($value, $option_id) {
                        $q->where('option_id', $option_id)->where('option_value_id', $value);

                    });
                }


            }
        }

        if ($request->price_start && !$request->price_end) {
            $products = $products->where('price', '>=', $request->price_start);
        }
        if (!$request->price_start && $request->price_end) {
            $products = $products->where('price', '<=', $request->price_end);
        }
        if ($request->price_start && $request->price_end) {
            $products = $products->whereBetween('price', array($request->price_start, $request->price_end));
        }


        $products = $products
            ->with(['ProductImage' => function ($q) {
                $q->select(['id', 'product_id', 'image']);
            }])->get();


        return response()->json($products);

    }

    public function search(Request $request)
    {
        $city_id = session('city_id');
        $products = Product::where('title', 'LIKE', "%{$request->search}%");
        if ($city_id) {
            $products = $products->where('city_id', $city_id);
        }
        $products = $products->get();
        if (session('country_id')) {

            $cities = City::where('country_id',session('country_id'))->get();
        }
        else{
            $country=Country::where('name_ar','السعودية')->first();
            $cities=City::where('country_id',$country->id)->get();
        }
        $categories = Category::where('parent_id', null)->get();

        $sliders = Slider::where('location', 2)->get();

        return view('website.products.search', compact('products', 'cities', 'categories', 'sliders'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSubCategories($id)
    {
        $category = Category::find($id);
        $categories = Category::where('parent_id', null)->get();
        $current_lang = App::getLocale();
        return view('website.products.categories', compact('category', 'current_lang', 'categories'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProduct($id)
    {
        $product = Product::find($id);
        $product->update([
            'views' => $product->views + 1
        ]);
        $similar_products = Product::where('category_id', $product->category_id)->where('id', '!=', $product->id)->get()->take(4);
        $most_watched_products = Product::orderBy('views', 'desc')->where('id', '!=', $product->id)->get()->take(4);


        $options = ProductOption::where('product_id',$id)->get();
        
    
        return view('website.products.product_details', compact('product', 'similar_products', 'most_watched_products'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProducts($id)
    {
        $ids = Category::where('parent_id', $id)->pluck('id')->toArray();
//        $country_id = session('country_id');
        $city_id = session('city_id');

        $products = Product::whereIn('category_id', $ids);
        if ($city_id) {
            $products = $products->where('city_id', $city_id);

        }
        $products = $products->get();
        $categories = Category::where('parent_id', null)->get();
        $current_lang = App::getLocale();

        $sliders = Slider::where('location', 3)->get();
        return view('website.products.products', compact('products', 'categories', 'current_lang', 'sliders'));
    }

    /**
     * @param
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function AddProduct()
    {
        $categories = Category::with('children')->where('parent_id', null)->get();
        $countries = Country::get();
        return view('website.products.add', compact('countries', 'categories'));
    }

    public function SaveProduct(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'phone' => 'required|string',
            'price' => 'required',
            'description' => 'sometimes|nullable|string',
            'type' => 'required',
            'city_id' => 'required',
            'options' => 'array',
        ]);
        $data = [
            'user_id' => auth()->id(),
            'title' => $request->title,
            'phone' => $request->phone,
            'price' => $request->price,
            'description' => $request->description,
            'category_id' => $request->type,
            'city_id' => $request->city_id,
        ];
        $product = Product::create($data);
        $product_id = $product->id;

        $images = json_decode(request()->images);
        if ($images) {
            if (count($images)) {
                foreach ($images as $image) {
                    $save_image = new ProductImage();
                    $save_image->product_id = $product_id;
                    $save_image->image = $image;
                    $save_image->save();
                }
            }
        }


        if (request()->value) {
            if (request()->value) {
                foreach (request()->value as $key => $option) {
                    if ($option) {
                        $dataa = new ProductOption();
                        $dataa->option_value_id = $option;
                        $dataa->product_id = $product_id;
                        $dataa->option_id = request()->option_id[$key];
                        $dataa->save();
                    }
                }
            }

        }


        session()->flash('success', 'تم تسجيل الاعلان بنجاح');
        return redirect(url('/'));
    }

    public function uploadAdImage(Request $request)
    {
        $data = $this->validate(request(), [
            'file' => 'required|max:1000',
        ], [], [
            'file' => 'logo',
        ]);
        if (request()->hasFile('file')) {
            $imageName = Str::random(10) . '.' . request()->File('file')->extension();
            request()->File('file')->move(
                base_path() . '/public/uploads/products/', $imageName
            );
            $data['file'] = '/uploads/products/' . $imageName;
        }
        echo $data['file'];
    }


}
