<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use App\Models\Fav;
use App\Models\Message;
use App\Models\Product;
use App\Notifications\NewMessageNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class UsersController extends Controller
{
    public function profile()
    {
        $user=Auth::user();
        $products=Product::where('user_id',$user->id)->get();
        return view('website.user.profile',compact('user','products'));

    }
    public function favourites()
    {
        $user=Auth::user();
        $ids=Fav::where('user_id',$user->id)->pluck('product_id')->toArray();
        $products=Product::whereIn('id',$ids)->get();
        return view('website.user.favourites',compact('products'));

    }

    public function chatDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'offer_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $to_user = User::where('id', $request->user_id)->first();

        $data = Message::select(['id', 'message', 'file', 'receiver_id', 'sender_id', 'offer_id', 'is_seen', 'created_at'])->where('offer_id', $request->offer_id)
            ->where(function ($query) use ($user, $to_user) {
                $query->where([['sender_id', $user->id], ['receiver_id', $to_user->id]]);
                $query->orWhere([['sender_id', $to_user->id], ['receiver_id', $user->id]]);
            })->orderBy('created_at', 'asc')->with(['sender' => function ($q) {
                $q->select(['id', 'name', 'tokens', 'photo'])->get();
            }]);
        $messages = $data->get();
        $messages->transform(function ($i) {
            $i->since = $i->created_at->diffForHumans();
            if ($i->file) {
                $i->file = \Helpers::base_url() . '/' . $i->file;
            }
            return $i;
        });

        //read messages
        $data->update(['is_seen' => 1]);

        $offer = Offer::find($request->offer_id);
        $invoice = Invoice::where('offer_id', $request->offer_id)->first();
        $order = \App\Models\Order::where('id', $offer->order_id)->select(['id', 'order_number', 'type', 'status', 'place_id'])->first();
        $order['store_name'] = $order->store ? (\Helpers::getLang() == 'ar' ? $order->store->name_ar : $order->store->name_en) : \Helpers::getPlaceCoordination($order->place_id)['name'];
        $order['store_image'] = $order->store ? \Helpers::base_url() . '/' . $order->store->logo : \Helpers::getPlaceCoordination($order->place_id)['icon'];
        $order['has_invoice'] = $invoice ? true : false;
        $captain = User::where('id', $offer->captain_id)->select(['id', 'name', 'phone', 'photo', 'latitude', 'langitude'])->first();
        $client = User::where('id', $offer->order->user_id)->select(['id', 'name', 'phone', 'photo', 'latitude', 'langitude'])->first();
        $captain['photo'] = $captain->photo ? \Helpers::base_url() . $captain->photo : '';
        $captain['latitude'] = (double)$captain->latitude;
        $captain['langitude'] = (double)$captain->langitude;
        $captain['delivary_cost'] = $offer->delivary_cost;
        $client['latitude'] = (double)$client->latitude;
        $client['langitude'] = (double)$client->langitude;
        $client['rate_degree'] = $client->user_rates->avg('degree') ?: 0;
        $client['photo'] = \Helpers::base_url() . $client->photo;
        unset($order['store']);
        unset($client['user_rates']);
        return response()->json([
            'messages' => $messages,
            'order' => $order,
            'captain' => $captain,
            'client' => $client
        ], 200);
    }
    public function sendMessage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'receiver_id' => 'required',
            'offer_id' => 'required',

        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        $to_user = User::where('id', $request->receiver_id)->first();

        $message = Message::create([
            'sender_id' => $user->id,
            'receiver_id' => $to_user->id,
            'message' => $request->message,
            'offer_id' => $request->offer_id,

        ]);

        if ($request->file) {
            $file = $request->file('file');

            $mediaName = bin2hex(random_bytes(10)) . date("YmdHis") . '.' . $file->getClientOriginalExtension();
            $file->move(
                base_path() . '/public/uploads/chats/', $mediaName
            );
            $message->update(['file' => '/uploads/chats/' . $mediaName]);
        }

        //notification
        $title = 'القيصر';
        $content = 'رسالة جديدة';
        $fcm_message = [
            'message_id' => (int)$message->id,
            'sender_id' => (int)$user->id,
            "is_seen" => (int)$message->is_seen,
            "message" => $message->message ?: null,
            "offer_id" => (int)$message->offer_id,
            'since' => $message->created_at->diffForHumans(),
            'created_at' => $message->created_at,
            'file' => \Helpers::base_url() . $message->file ?: null,
            'type' => 'message',
            "sender" => [
                'id' => $user->id,
                'name' => $message->sender->name ?: "",
                'photo' => \Helpers::base_url() . $message->sender->photo ?: "",
                'tokens' => $message->sender->tokens,
            ]
        ];

        Notification::send($to_user, new NewMessageNotification($message));
        \Helpers::fcm_notification($to_user->device_token, $content, $title, $fcm_message);

        return response()->json([], 204);
    }

    public function chats(){
        $user=Auth::user();
        $received_messages =Message::select('*')->
        from(DB::raw("
    ( 
      SELECT *,`sender_id` as `theuser`
      FROM `messages` WHERE `receiver_id` = $user->id
      union
      SELECT *,`receiver_id` as `theuser`
      FROM `messages` WHERE `sender_id` = $user->id
      order by `created_at` desc
    ) as `sub`"))->groupBy('theuser')->
        orderBy('created_at', 'desc')->
        get();
//            Message::where('receiver_id', $user->id)->orderBy('created_at', 'desc')
//            ->select('id','sender_id','receiver_id','message','created_at')->groupBy('sender_id')->get();


        if ($received_messages->isNotEmpty())
        {
            $recent_chat = Message::where([['sender_id', $received_messages[0]->sender_id], ['receiver_id', $received_messages[0]->receiver_id]])
                ->orWhere(function ($query) use ($user, $received_messages) {
                    return $query->where([['sender_id', $received_messages[0]->receiver_id], ['receiver_id', $received_messages[0]->sender_id]]);
                })->get();
        }else{
            $recent_chat=collect();
        }


        return view('website.user.chat',compact('received_messages','recent_chat','user'));

    }



}
