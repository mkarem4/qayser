<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Option;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class SubCategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::where('parent_id', '<>', null)->orderBy('created_at', 'ASC')->paginate(10);
        return view('admin.subcategories.index', compact('categories'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = Category::get();
        $main_categories = Category::whereHas('children')->get();
        return view('admin.subcategories.add', compact('categories', 'main_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request->validate([
            'name_ar' => 'required',
            'name_en' => 'required',
//            'icon' => 'required',
        ]);

        $category = Category::create($request->except('icon', 'title_ar', 'title_en', 'option_icon'));

        if ($request->hasFile('icon')) {
            $request->validate([
                'icon' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $imageName = Str::random(10) . '.' . $request->file('icon')->extension();
            $request->file('icon')->move(
                base_path() . '/public/uploads/categories/', $imageName
            );
            $category->icon = '/uploads/categories/' . $imageName;
            $category->save();
        }
        $category_id = $category->id;

        if (request()->title_ar[0]) {
            foreach (request()->title_ar as $key => $title) {
                $level = new Option();
                $level->category_id = $category_id;
                $level->name_ar = $title;
                $level->name_en = request()->title_en[$key];
                $level->save();
                if ($request->file('option_icon')) {
                    if (array_key_exists($key,$request->file('option_icon'))) {
                        $imageName = Str::random(10) . '.' . $request->file('option_icon')[$key]->extension();
                        $request->file('option_icon')[$key]->move(
                            base_path() . '/public/uploads/', $imageName
                        );

                        $level->icon = '/uploads/' . $imageName;
                        $level->save();
                    }
                }
            }
        }
        return redirect('/webadmin/subcategories')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم اضافة القسم بنجاح']));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $categories = Category::whereHas('children')->get();
        $option = Option::where('category_id', $id)->get();
        return view('admin.subcategories.edit', compact('category', 'categories', 'option'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name_ar' => 'required',
            'name_en' => 'required',
        ]);
        $category = Category::find($id);
        $category->update($request->except('icon', 'title_ar', 'title_en', 'option_id', 'option_icon'));
        if ($request->hasFile('icon')) {
            $request->validate([
                'icon' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);
            $imageName = Str::random(10) . '.' . $request->file('icon')->extension();
            $request->file('icon')->move(
                base_path() . '/public/uploads/categories/', $imageName
            );
            $category->icon = '/uploads/categories/' . $imageName;
            $category->save();
        }
        if (count(request()->title_ar) > 0) {
            foreach (request()->title_ar as $key => $title) {
                if ($title) {
                    $level = Option::updateOrCreate(
                        ['category_id' => $id, 'id' => request()->option_id[$key] ?? null],
                        ['name_ar' => $title, 'name_en' => request()->title_en[$key] ?? '']
                    );
                    if ($request->file('option_icon')) {
                        if (array_key_exists($key,$request->file('option_icon'))) {
                            $imageName = Str::random(10) . '.' . $request->file('option_icon')[$key]->extension();
                            $request->file('option_icon')[$key]->move(
                                base_path() . '/public/uploads/', $imageName
                            );
                            $level->icon = '/uploads/' . $imageName;
                            $level->save();
                        }
                    }
                } else {
                    Option::where('id', request()->option_id[$key] ?? 0)->delete();
                }
            }
        }
        return redirect('/webadmin/subcategories')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم تعديل القسم بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::destroy($id);
        return redirect('/webadmin/subcategories')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم حذف القسم بنجاح']));
    }
}
