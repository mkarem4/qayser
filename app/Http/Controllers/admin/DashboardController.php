<?php

namespace App\Http\Controllers\admin;


use App\Models\Category;
use App\Models\Choice;
use App\Models\Company;
use App\Models\Contact;
use App\Models\ContractRequest;
use App\Models\Maintenance;
use App\Models\Meeting;
use App\Models\Offer;
use App\Models\PriceRequest;
use App\Models\Product;
use App\Models\RequestService;
use App\Models\Setting;
use App\Models\Startup;
use App\User;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(){

        $users=User::where('role','<>',1)->count();
        $admins=User::where('role',1)->count();
        $categories=Category::where('parent_id',null)->count();
        $products=Product::count();
        $setting=Setting::first();


        return view('admin.index',compact('users','products','admins','categories','setting'));
    }
}
