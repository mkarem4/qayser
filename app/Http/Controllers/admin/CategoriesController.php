<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Option;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::where('parent_id', null)->orderBy('created_at', 'ASC')->paginate(10);
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = Category::get();
        $main_categories = Category::where('parent_id', null)->get();
        return view('admin.categories.add', compact('categories', 'main_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_ar' => 'required',
            'name_en' => 'required',
            'icon' => 'required',
        ]);

        $category = Category::create($request->except('icon', 'title_ar', 'title_en'));

        if ($request->hasFile('icon')) {
            $request->validate([
                'icon' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $imageName = Str::random(10) . '.' . $request->file('icon')->extension();
            $request->file('icon')->move(
                base_path() . '/public/uploads/categories/', $imageName
            );
            $category->icon = '/uploads/categories/' . $imageName;
            $category->save();
        }

        return redirect('/webadmin/categories')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم اضافة القسم بنجاح']));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $main_categories = Category::where('parent_id', null)->get();
        $option = Option::where('category_id', $id)->get();
        return view('admin.categories.edit', compact('category', 'main_categories', 'option'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name_ar' => 'required',
            'name_en' => 'required',
        ]);
        $category = Category::find($id);
        $category->update($request->except('icon', 'title_ar', 'title_en', 'option_id'));
        if ($request->hasFile('icon')) {
            $request->validate([
                'icon' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);
            $imageName = Str::random(10) . '.' . $request->file('icon')->extension();
            $request->file('icon')->move(
                base_path() . '/public/uploads/categories/', $imageName
            );
            $category->icon = '/uploads/categories/' . $imageName;
            $category->save();
        }

        return redirect('/webadmin/categories')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم تعديل القسم بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::destroy($id);
        return redirect('/webadmin/categories')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم حذف القسم بنجاح']));
    }
}
