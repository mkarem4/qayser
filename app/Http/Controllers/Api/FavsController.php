<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Fav;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FavsController extends Controller
{
    public function addToFav(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $fav = Fav::where('user_id', $user->id)->where('product_id', $request->product_id)->first();
        if ($fav) {
            $fav->delete();
            return response()->json([], 204);
        } else {
            Fav::create([
                'user_id' => $user->id,
                'product_id' => $request->product_id,
            ]);

            return response()->json([], 204);
        }
    }

    public function getFavs()
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $ids = Fav::where('user_id', $user->id)->pluck('product_id')->toArray();
        $products = Product::whereIn('id', $ids)->select(['id', 'title', 'price'])->get();
        $products->transform(function ($i) {
            $i->image = \Helpers::base_url() . '/' . ProductImage::where('product_id', $i->id)->first()->image;
            return $i;

        });
        return response()->json(['favs' => $products], 200);
    }
}
