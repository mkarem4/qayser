<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductDetails extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $photos = [];
        $opts = [];
        $options = [];
        foreach ($this->ProductImage as $photo) {
            $photos[] = \Helpers::base_url() . $photo->image;
        }

        foreach ($this->ProductOption as $key => $value) {
            $opts['option'] = \Helpers::getLang() == 'ar' ? $value->option->name_ar : $value->option->name_en;
            $opts['value'] = $value->value;
            array_push($options, $opts);
        }

        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'views' => (int)$this->views,
            'price' => (double)$this->price,
            'user' => $this->user->username,
            'user_id' => $this->user->id,
            'phone' => $this->phone,
            'category' => App()->getLocale() == 'ar' ? $this->category->name_ar : $this->category->name_en,
            'city' => App()->getLocale() == 'ar' ? $this->city->name_ar : $this->city->name_en,
            'photos' => $photos,
            'options' => $options,
            'is_fav' => $this->isFav(\Helpers::getLoggedUser()->id, $this->id)
        ];
    }
}
