<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-line-graph"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الرئيسية</span>
            </span>
        </span>
    </a>
</li>

<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/settings/1/edit')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-settings"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاعدادات</span>
            </span>
        </span>
    </a>
</li>



<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/users')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-user"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاعضاء</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/admins')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-users-1"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المدراء</span>
            </span>
        </span>
    </a>
</li>

<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/countries')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-home-2"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الدول</span>
            </span>
        </span>
    </a>
</li>

<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/cities')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-home-1"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المدن</span>
            </span>
        </span>
    </a>
</li>


<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/categories')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-technology-1"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاقسام الرئيسيه</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/subcategories')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-technology-1"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاقسام الفرعيه</span>
            </span>
        </span>
    </a>
</li>

<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/categories-options')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-technology-1"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">خصائص الاقسام</span>
            </span>
        </span>
    </a>
</li>


<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/products')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المنتجات</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/sliders')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-shapes"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">البنرات الاعلانية</span>
            </span>
        </span>
    </a>
</li>

