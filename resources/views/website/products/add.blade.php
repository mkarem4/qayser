@extends("website.layouts.app")
@section('content')


    @push('css')
        <link rel="stylesheet" href="{{asset('website/js/dropzone')}}/dropzone.css">
        <link rel="stylesheet" href="{{asset('website/js/dropzone')}}/basic.css">
    @endpush
    <div class="add-adv1 addv4">
        <div class="container">
            <h4>» ارفع الاعلان </h4>
            <br>
            <form class="basic-form" id="basic-form" method="post" action="{{ url('add-ad') }}"
                  enctype="multipart/form-data" autocomplete="off">
                @csrf
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">اختر القسم</label>
                    <div class="col-sm-10">
                        <select class="form-control" required id="category_id" name="category_id">
                            <option value="" selected="selected">اختر القسم</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name_ar}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">اختر نوع الاعلان</label>
                    <div class="col-sm-10">
                        <select class="form-control" required id="subcategory_id" name="subcategory_id">
                            <option value="" selected="selected">اختر نوع الاعلان</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">اختر مواصفات الاعلان</label>
                    <div class="col-sm-10">
                        <select class="form-control" required id="type" name="type">
                            <option value="" selected="selected">اختر مواصفات الاعلان</option>
                        </select>
                    </div>
                </div>
                <div class="form-group  row" id="options_div" style="display: none">
                    <label class="col-sm-2 col-form-label">خصائص القسم </label>
                    <div class="col-sm-10">
                        <div class="table-responsive">
                            <table class="table m-0">
                                <thead>
                                <tr>
                                    <th width="10%">#</th>
                                    <th width="10%">الخاصيه</th>
                                    <th>القيمه</th>
                                </tr>
                                </thead>
                                <tbody id="table_dynamic">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="form-group  row">
                    <label for="city_id" class="col-sm-2 col-form-label">الدوله </label>
                    <div class="col-sm-10{{ $errors->has('main_category') ? ' has-danger' : '' }}">
                        <select name="country_id" id="country_id" class="form-control country_id m-input" required>
                            <option value="">قم باختيار الدوله</option>
                            @foreach($countries as $country)
                                <option
                                        value="{{$country->id}}" {{old('city_id')==$country->id ?'selected':''}}>{{ $country->name_ar }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group  row">
                    <label for="city_id" class="col-sm-2 col-form-label">المدينه </label>
                    <div class="col-sm-10{{ $errors->has('main_category') ? ' has-danger' : '' }}">
                        <select name="city_id" id="city_id" class="form-control city_id m-input" required>
                            {{--<option value="">قم باختيار المدينه</option>--}}
                            {{--@foreach($cities as $city)--}}
                                {{--<option--}}
                                    {{--value="{{$city->id}}" {{old('city_id')==$city->id ?'selected':''}}>{{ $city->name_ar }}</option>--}}
                            {{--@endforeach--}}
                        </select>
                    </div>
                </div>

                <div class="form-group  row">
                    <label class="col-sm-2 col-form-label">اسم الاعلان </label>
                    <div class="col-sm-10{{ $errors->has('title') ? ' has-danger' : '' }}">
                        {!! Form::text('title',null,['class'=>'form-control m-input','required','autofocus','placeholder'=>"ادخل اسم الاعلان"]) !!}
                    </div>
                </div>

                <div class="form-group  row">
                    <label class="col-sm-2 col-form-label">رقم الجوال </label>
                    <div class="col-sm-10{{ $errors->has('phone') ? ' has-danger' : '' }}">
                        {!! Form::number('phone',null,['class'=>'form-control m-input','required','autofocus','placeholder'=>"رقم الجوال "]) !!}
                    </div>
                </div>

                <div class="form-group row" id="price">
                    <label class="col-sm-2 col-form-label">السعر </label>
                    <div class="col-sm-10{{ $errors->has('price') ? ' has-danger' : '' }}">
                        {!! Form::number('price',null,['class'=>'form-control m-input','autofocus','required','placeholder'=>" السعر"]) !!}
                    </div>
                </div>


                <div class="form-group  row">
                    <label class="col-sm-2 col-form-label">وصف الاعلان </label>
                    <div class="col-sm-10{{ $errors->has('description') ? ' has-danger' : '' }}">
                        {!! Form::textarea('description',null,['class'=>'form-control m-input','autofocus','required','placeholder'=>"وصف الاعلان"]) !!}
                    </div>
                </div>

                <div class="form-group  row">
                    <input type="hidden" id="images" name="images">
                    <label class="col-sm-2 col-form-label">ارفع الصور </label>

                    <div class="col-sm-10 dropzone" id="mydropzone">
                        <div class="fallback">
                            <input type="file" name="file"/>
                            @csrf
                        </div>
                        <p>
                            <small> * قم بالقاء الصور المراد تحميلها داخل الصندوق عن طريق خاصيه السحب والافلات او قم
                                بالضغط على المربع للتحميل</small><br/>
                            <small> * الصور يجب ان تكون من صيغه jpg, jpeg, png, gif</small><br/>
                            <small> * اقصي حجم للصوره الواحده 500 كيلو بايت </small>
                        </p>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="submit">ارفع الاعلان</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('js')
    <script>

        $('#category_id').on('change', function () {
            if ($(this).val() == 85) {
                $('#price').hide(200);
            } else {
                $('#price').show(200);
            }
        });

        $('#subcategory_id').on('change', function () {
            $('#options_div').hide(200);
            $('#table_dynamic').html('');
            document.getElementById('type').value = '';
            if ($(this).val()) {
                LoadOptions.call(this);
            }
        });

        $('#type').on('change', function () {
            $('#options_div').hide(200);
            $('#table_dynamic').html('');
            if ($(this).val()) {
                LoadOptions.call(this);
            }
        });

        function LoadOptions(category) {
            let subcategory =  document.getElementById('type').value ;
            let Maincategory =  document.getElementById('subcategory_id').value ;
            let url = '{{url('api/get-category-options/')}}?category[]=' + Maincategory +'&category[]=' +subcategory;
            $.ajax({
                url: url,
                beforeSend: function (request) {
                    request.setRequestHeader("Accept-Language", 'ar');
                },
            }).done(function (data) {
                if (data.length > 0) {
                    $('#options_div').show(200);
                }
                $.each(data, function (index, val) {
                    let newdiv = document.createElement('tr');

                    let values = [];
                    $.each(val.option, function (index, value) {
                        values.push('<option value="' + value.id + '">' + value.name + '</option>')

                    });
                    newdiv.innerHTML = "<td>#<input type='hidden' readonly name='option_id[]' value=" + val.id + "></td>" +
                        "<td>" + val.name + "</td>" +
                        "<td>" +
                        "<select class='form-control' name='value[]'><option value='' >قم بالاختيار </option>" +
                        values +
                        "</select></td>";
//
//
//                    newdiv.innerHTML = "<td>#<input type='hidden' readonly name='option_id[]' value=" + val.id + "></td>" +
//                        "<td>" + val.name + "</td>" +
//                        "<td><input name='value[]' class='form-control' type='text' ></td>";
                    document.getElementById('table_dynamic').appendChild(newdiv);
                });
            });
        }
    </script>
    <script src="{{asset('website/js/dropzone/dropzone.js')}}"></script>

    <script>

        jQuery(document).ready(function () {
            Dropzone.autoDiscover = false;
            let images = [];


            $("div#mydropzone").dropzone({
                url: "{{url('add-ad/image-ajax-upload')}}",
                rtl: true,
                maxFilesize: 1,
                maxFiles: 10,
                timeout: 5000,
                dictDefaultMessage: 'قم بسحب وافلات الصور هنا للرفع',
                init: function () {
                    this.on("sending", function (file, xhr, formData) {
                        formData.append("_token", "{{csrf_token()}}");
                    });
                },
                success: function (file, response) {
                    images.push(response);
                    $("input#images").val(JSON.stringify(images))
                },

            });
            let Delete = function (id) {
                $("#loaderDiv").show();
                $.ajax({

                    type: "POST",
                    url: "{{url('account/ads/images/Delete')}}",
                    data: {id: id, _token: '{{csrf_token()}}'},
                    success: function (result) {
                        $("#loaderDiv").hide();
                        $("#ExprojDel").modal("hide");
                        $("#image_" + id).remove();
                    }

                })
            };

        });


    </script>

@endpush

