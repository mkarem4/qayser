@extends("website.layouts.app")
@section('content')

    @if(isset($sliders) && count($sliders) > 0)
        <section>
            <div class="container">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        @foreach($sliders as $slider)
                            <div class="item active">
                                <img src="{{$slider->photo}}" alt="Los Angeles" style="width:100%; height: 250px">
                            </div>
                        @endforeach
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </section>
    @endif
    <div class="container hide-in-sm">
        <section class="banner" id="top">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 pull-left">
                        <div class="banner-caption">
                            <div class="line-dec"></div>
                            <span>من خلال فرصة بإمكانك البحث عن وظيفة وبيع وشراء كل ما تحتاجه</span>
                            <!-- <div class="add-adv">
                                <a href="#">اضف اعلانك الان</a>
                            </div> -->
                        </div>
                        <div class="submit-form">
                            <div class="tab-content" id="pills-tabContent">

                                <div class="tab-pane fade in active" id="pills1" role="tabpanel"
                                     aria-labelledby="pills-contact-tab">
                                    <form action="/search" method="post">
                                        @csrf

                                        <div class="row">
                                            <div class="col-md-9 first-item">
                                                <fieldset>
                                                    <input name="search" type="text" class="form-control" id="name"
                                                           placeholder="ابحث عن">
                                                </fieldset>
                                            </div>

                                            <div class="col-md-3">
                                                <fieldset>
                                                    <button type="submit" id="form-submit" class="btn">ابحث</button>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <section class="our-services" id="services">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading hide-in-sm">
                        <h2>الاقسام</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($categories as $category)
                    <div class="col-md-3 col-xs-4">
                        <div class="service-item">
                            <h4>
                                <img src="{{$category->icon}}" alt="" width="16" height="16">
                                <a href="/categories/{{$category->id}}">{{$current_lang == 'ar' ?$category->name_ar: $category->name_en}}</a>
                            </h4>

                            <ul class="hide-in-sm">
                                @foreach($category->children->take(5) as $child)
                                    <li><a href="/products/{{$child->id}}"><i
                                                class="fa fa-angle-left"></i>{{$current_lang == 'ar' ?$child->name_ar: $child->name_en}}
                                        </a></li>
                                @endforeach
                            </ul>

                            <div class="more hide-in-sm">
                                <a href="/categories/{{$category->id}}">المزيد
                                    من {{$current_lang == 'ar' ?$category->name_ar: $category->name_en}}</a>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="popular-places" id="popular">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2>الاعلانات الاكثر زيارة</h2>
                    </div>
                </div>
            </div>
            <div class="owl-carousel owl-theme sliderrr sliderrr" dir="ltr">
                @foreach($most_viewed_products as $most_product)
                    <div class="item popular-item">
                        <div class="thumb">
                            <a href="/product/{{$most_product->id}}"><img
                                    src="{{asset($most_product->ProductImage[0]->image ??'')}}" alt=""></a>
                            <div class="text-content">
                                <a href="/product/{{$most_product->id}}">
                                    <h4>{{$most_product->title}}</h4>
                                </a>
                                @if($most_product->price)
                                    <span>{{$most_product->price}} ريال</span>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="popular-places" id="popular">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2>احدث الاعلانات</h2>
                    </div>
                </div>
            </div>
            <div class="owl-carousel owl-theme sliderrr" dir="ltr">
                @foreach($latest_products as $latest_product)
                    <div class="item popular-item">
                        <div class="thumb">
                            <a href="/product/{{$latest_product->id}}"><img
                                    src="{{asset($latest_product->ProductImage[0]->image ?? '')}}" alt=""></a>
                            <div class="text-content">
                                <a href="/product/{{$latest_product->id}}">
                                    <h4>{{$latest_product->title}}</h4>
                                </a>
                                @if($latest_product->price)
                                    <span>{{$latest_product->price}} ريال</span>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>





@endsection
