@extends('website.layouts.app')

@section('content')
    <div class="padding-top-70 padding-bottom-90 access-page-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <div class="row">
                                <form class="" action="{{route('password.email')}}" method="post">
                                    @csrf
                                <div class="col-xl-12 col-md-12">
                                    <div class="access-form">

                                            <div class="card">
                                                <div class="card-body">
                                                    <h6 class="card-title">Reset Password</h6>
                                                    <div class="form-group row">
                                                        <label for="email" class="col-md-3 col-form-label input-style">Email</label>
                                                        <div class="col-sm-6">
                                                            <input type="email" class="form-control" id="text" name="email">
                                                            @if ($errors->has('email'))
                                                                <span class="help-block">
                                                                   <strong style="color: red;">
                                                                       {{ $errors->first('email') }}
                                                                   </strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-sm-6 offset-3">
                                                            <p6 class="rst-pswrd">An email with the reset password instructions will be sent to the above email.</p6>
                                                        </div>

                                                    </div>


                                    </div>
                                </div>
                                <div class="row centr-btn">
                                    <button class="button primary-bg btn-block" type="submit">Reset</button>
                                </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    </div>
    </div>
@endsection
